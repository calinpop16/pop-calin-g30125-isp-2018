package g30125.pop.calin.l3.e6;

public class TestMyPoint {
	
	public static void main (String [] args) {
		//Testele pentru constructori
		MyPoint p1 = new MyPoint();
		MyPoint p2 = new MyPoint();
		MyPoint p3 = new MyPoint(2,3);
		
		//Testele pentru metoda toString()
		System.out.println(p1);
		System.out.println(p2);
		System.out.println(p3);
		
		//Test setter
		p2.setX(1);
		p2.setY(1);
		System.out.println(p2);
		
		//Test getter
		System.out.println("x este: "+p2.getX());
		System.out.println("y este: "+p2.getY());
		
		//Test metoda setXY
		p2.setXY(5, 7);
		System.out.println(p2);
		
		//Teste pentru metodele de calculare a distantei
		System.out.println(p1.distance(1, 3));
		System.out.println(p1.distance(p1));
		System.out.println(p1.distance(p3));
	}

}
