package g30125.pop.calin.l5.e2;

public class RotatedImage implements Image{
	private String fileName;
	
	   public RotatedImage(String fileName){
		      this.fileName = fileName;
		   
		   }
	@Override
	public String display() {
		return ("Display rotated "+this.fileName);
	}

}