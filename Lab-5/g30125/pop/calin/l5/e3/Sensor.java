package g30125.pop.calin.l5.e3;

public abstract class Sensor {
protected String location;
	
	abstract int readValue();
	public String getLocation() {
		return this.location;
	}

}
