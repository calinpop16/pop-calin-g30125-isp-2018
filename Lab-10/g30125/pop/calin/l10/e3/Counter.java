package g30125.pop.calin.l10.e3;



class Counter extends Thread
{
      String n;
      Thread t;
      Counter(String n, Thread t){this.n = n;this.t=t;}
 
      public void run()
      {int n1=0,n2=10,x=1;
    	          try
            {                
                  if (t!=null) { t.join();n1=10;n2=21;x=2;}
                  for(int i=n1;i<n2;i++){
                      System.out.println(" i = "+i);}
                  Thread.sleep((int)(Math.random() * 1000));
                  System.out.println("Counter "+x+" finished it's job.");
                  
            }
            catch(Exception e){e.printStackTrace();} 
    	  }
 
      
 
public static void main(String[] args)
{
      Counter w1 = new Counter("Counter 1",null);
      Counter w2 = new Counter("Counter 2",w1);
      w1.start();
      w2.start();
}
}