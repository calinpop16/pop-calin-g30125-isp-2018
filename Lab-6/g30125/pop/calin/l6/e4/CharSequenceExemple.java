package g30125.pop.calin.l6.e4;

import java.util.Arrays;

public class CharSequenceExemple implements CharSequence{

	private char [] chars;
	
	public CharSequenceExemple(char[] chars) {
		super();
		this.chars = chars;
	}
	

	@Override
	public char charAt(int id) {
		return chars[id];
	}

	@Override
	public int length() {
		return chars.length;
	}

	@Override
	public CharSequence subSequence(int arg0, int arg1) {
		char [] newc=Arrays.copyOfRange(chars, arg0, arg1);
		return new CharSequenceExemple(newc); 
	}

}
