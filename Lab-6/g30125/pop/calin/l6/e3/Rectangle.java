package g30125.pop.calin.l6.e3;


import java.awt.*;

public class Rectangle implements Shape{

    private int length,x,y;
    private Color color;
    String id;
    boolean filled;

    public Rectangle(Color color, int length,int x, int y,String id,boolean filled) {
        this.color=color;
        this.x=x;
        this.y=y;
        this.id=id;
        this.filled=filled;
        this.length = length;
    
    }
    
    public int getLength() {
        return length;
    }
    
   

    public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}
    @Override
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public boolean isFilled() {
		return filled;
	}

	public void setFilled(boolean filled) {
		this.filled = filled;
	}

	public void setLength(int length) {
		this.length = length;
	}

	@Override
    public void draw(Graphics g) {
        System.out.println("Drawing a rectangel "+length+" "+getColor().toString());
        g.setColor(getColor());
        g.drawRect(getX(),getY(),length,length);
        if(isFilled()==true)
        	g.fillRect(getX(), getY(), length, length);
        
    }
    
    
}