package g30125.pop.calin.l2.e3;

import java.util.Scanner;

public class Ex3 {
	static int prim(int n) {
		int d=2;
		while(d*d<=n){
			if (n%d==0) return 0;
		    d++;
		    }
		return 1;
		
	}
	public static void main(String [] args) {
		Scanner in = new Scanner(System.in);
		   System.out.println("Intoduceti numerele:");
		   int x=in.nextInt();
		   int y=in.nextInt();
		   int i,nr=0;
		   for (i=x;i<=y;i++)
		   if(prim(i)==1)
		   {
			   nr++;
			   System.out.println(" "+i);
		   }
		   System.out.println("\n Numarul de numere prime din interval este: "+nr);
		   
		   in.close();
	}
}
