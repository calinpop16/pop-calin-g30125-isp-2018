package g30125.pop.calin.l2.e6;

import java.util. Scanner;
public class Ex6 {

	static int non_recursiv (int n){
		int i,fact=1;
		for(i=1;i<=n;i++)
			fact=fact*i;
		return fact;
	}
	static int recursiv (int n) {
		if(n==1) return 1;
		
			return n*recursiv(n-1);
	}
	public static void main (String [] args) {
		Scanner in=new Scanner(System.in);
		System.out.println("Introduceti n: ");
		int n=in.nextInt();
		System.out.println("Introduceti metoda dorita: 1.non_recursiv, 2.recursiv");
		int ok=in.nextInt();
		if(ok==1)
		{
			System.out.println("Factorialul este: "+non_recursiv(n));
		}
		else
		{
			System.out.println("Factorialul este "+recursiv(n));
		}
		
		in.close();
	}
}
