package g30125.pop.calin.l2.e2;

import java.util.Scanner;

public class Ex2 {
	   static void a(int x) {
		   if (x==1) System.out.println("Unu");
		   else if (x==2) System.out.println("Doi");
		   else if (x==3) System.out.println("Trei");
		   else if (x==4) System.out.println("Patru");
		   else if (x==5) System.out.println("Cinci");
		   else if (x==6) System.out.println("Sase");
		   else if (x==7) System.out.println("Sapte");
		   else if (x==8) System.out.println("Opt");
		   else if (x==9) System.out.println("Noua");
		   else System.out.println("Altul");
	   
		   
	   }
	   static void b(int x) {
		   switch (x)
		   {
			   case 1:{ System.out.println("Unu"); break;}
			   case 2:{ System.out.println("Doi"); break;}
			   case 3:{System.out.println("Trei"); break;}
			   case 4:{System.out.println("Patru"); break;}
			   case 5:{ System.out.println("Cinci"); break;}
			   case 6:{ System.out.println("Sase"); break;}
			   case 7:{ System.out.println("Sapte"); break;}
			   case 8:{ System.out.println("Opt"); break;}
			   case 9:{ System.out.println("Noua"); break;}
			   default:{System.out.println("Altul"); break;}
		   }
		   
	   }
		public static void main (String [] args) {
			Scanner in = new Scanner(System.in);
			   System.out.println("Intoduceti numarul:");
			   int x=in.nextInt();
			   System.out.println("Metoda: 1-if/else; 2-switch");
			   int y=in.nextInt();
			   if(y==1) a(x);
			   else 
			      b(x);
			  in.close(); 
		}
}
