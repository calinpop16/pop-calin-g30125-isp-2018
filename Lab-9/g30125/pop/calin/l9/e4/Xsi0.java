package g30125.pop.calin.l9.e4;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JToggleButton;

public class Xsi0 extends JFrame {
	String startGame="X";
    JToggleButton button1;
    JToggleButton button2;
    JToggleButton button3;
    JToggleButton button4;
    JToggleButton button5;
    JToggleButton button6;
    JToggleButton button7;
    JToggleButton button8;
    JToggleButton button9;
    Xsi0(){
        setTitle("X&0");
       setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
       init();
       setSize(210,250);
       setVisible(true);
    }
    public void init(){
    	int turn=0;
         this.setLayout(null);
            int width=80;int height = 20;
            button1=new JToggleButton("");
            button1.setBounds(10, 10, 50, 50);
            button1.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					button1.setText(startGame);
					button1.setEnabled(false);
					choosePlayer();
					
				}
            	
            });
            button2=new JToggleButton("");
            button2.setBounds(10, 70, 50, 50);
            button2.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					button2.setText(startGame);
					button2.setEnabled(false);
					choosePlayer();
					
				}
            	
            });
            button3=new JToggleButton("");
            button3.setBounds(10, 130, 50, 50);
            button3.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					button3.setText(startGame);
					button3.setEnabled(false);
					choosePlayer();
					
				}
            	
            });
            button4=new JToggleButton("");
            button4.setBounds(70, 10, 50, 50);
            button4.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					button4.setText(startGame);
					button4.setEnabled(false);
					choosePlayer();
					
				}
            	
            });
            button5=new JToggleButton("");
            button5.setBounds(70, 70, 50, 50);
            button5.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					button5.setText(startGame);
					button5.setEnabled(false);
					choosePlayer();
					
				}
            	
            });
            button6=new JToggleButton("");
            button6.setBounds(70, 130, 50, 50);
            button6.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					button6.setText(startGame);
					button6.setEnabled(false);
					choosePlayer();
					
				}
            	
            });
            button7=new JToggleButton("");
            button7.setBounds(130, 10, 50, 50);
            button7.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					button7.setText(startGame);
					button7.setEnabled(false);
					choosePlayer();
					
				}
            	
            });
            button8=new JToggleButton("");
            button8.setBounds(130, 70, 50, 50);
            button8.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					button8.setText(startGame);
					button8.setEnabled(false);
					choosePlayer();
					
				}
            	
            });
            button9=new JToggleButton("");
            button9.setBounds(130, 130, 50, 50);
            button9.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					button9.setText(startGame);
					button9.setEnabled(false);
					choosePlayer();
					
				}
            	
            });
            add(button1);
            add(button2);
            add(button3);
            add(button4);
            add(button5);
            add(button6);
            add(button7);
            add(button8);
            add(button9);
            
    }
    public static void main(String[]args){
        new Xsi0();
    }
    public void choosePlayer() {
    	if(startGame.equalsIgnoreCase("X")) {
    		startGame="0";
    	}
    	else {
    		startGame="X";
    	}
    }

        }