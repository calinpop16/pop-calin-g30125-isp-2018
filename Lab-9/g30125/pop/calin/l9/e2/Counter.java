package g30125.pop.calin.l9.e2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Counter extends JFrame{
	JTextArea text;
	 JButton b;
	
	Counter(){
		setTitle("Counter");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(200,300);
        setVisible(true);
	}
	  public void init(){
		  
          this.setLayout(null);
          int width=200;int height = 20;

         
          text = new JTextArea();
          text.setBounds(70,50,width, height);


          b = new JButton("Increment");
          b.setBounds(10,150,width, height);

          b.addActionListener(new TratareButon());
          
          add(text);
          add(b);

    }
	  public static void main(String[] args) {
          new Counter();
    }


class TratareButon implements ActionListener{
	 private int counter=0;
    public void actionPerformed(ActionEvent e) {
    	Counter.this.text.setText("");
    	Counter.this.text.append("counter: "+counter);
    	counter++;
    	
    }
}
}
