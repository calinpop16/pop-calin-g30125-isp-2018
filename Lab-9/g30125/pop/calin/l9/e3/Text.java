package g30125.pop.calin.l9.e3;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

import javax.swing.*;

import g30125.pop.calin.l9.e2.Counter;

public class Text extends JFrame{
	JTextArea atext;
	 JButton b;
	 JTextField ftext;
	 
	 
	 Text(){
			setTitle("Text");
	        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	        init();
	        setSize(400,500);
	        setVisible(true);
		}
	 public void init(){
		  
         this.setLayout(null);
         int width=200;int height = 20;

        
         atext = new JTextArea();
         atext.setBounds(10,180,150,80);

         ftext= new JTextField();
         ftext.setBounds(70,50,width, height);

         b = new JButton("Display file content");
         b.setBounds(10,150,width, height);

         b.addActionListener(new TratareButon());
         
         add(atext);
         add(b);
         add(ftext);

   }
	 public static void main(String[] args) {
         new Text();
   }
	 class TratareButon implements ActionListener{
		
         public void actionPerformed(ActionEvent e)       {

             String text = Text.this.ftext.getText();
             Text.this.atext.setText("");

             try {
                 Scanner input;
                 File file = new File(text);
                 if(file.exists()) {
                     input = new Scanner(file);


                     while (input.hasNextLine()) {
                         String line = input.nextLine();
                         Text.this.atext.append(line);
                     }
                     input.close();
                 }
                 else  Text.this.atext.append("Fisierul nu exista");


             } catch (Exception ex) {
                 ex.printStackTrace();
             }

         }
	 }
         

}
