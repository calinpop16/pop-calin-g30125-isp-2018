package g30125.pop.calin.l4.e6;

public class Book {
	private String name;
	private Author[] author;
	private double price;
	private int qtyInStock;
	private int n;
	Book(String name,Author[] author,Double price,int n){
		this.name=name;
		author=new Author[n];
		for (int i=0;i<n;i++)
			this.author=author;
		this.price=price;
		this.n=n;
		
	}
	
	Book(String name,Author[] author,Double price,int n,int qtyInStock){
		this.name=name;
		author=new Author[n];
		for (int i=0;i<n;i++)
			this.author=author;
		this.price=price;
		this.qtyInStock=qtyInStock;
		this.n=n;
		
	}
	
	public String get_name() {
		return this.name;
	}
	public Author[] get_author()
	{
		return this.author;
	}
	public double get_price() {
		return this.price;
	}
	
	public void set_price(double price) {
		this.price=price;
	}
	public int get_qtyInStock() {
		return this.qtyInStock;
	}
	public void set_qtyInStock(int qtyInStock) {
		this.qtyInStock=qtyInStock;
	}
	public String toString() {
		return "book-"+get_name()+" by "+this.n+" authors";
	}
	public void printAuthors() {
		for(int i=0;i<this.n;i++)
			System.out.println(author[i].get_name()+" ");
	}


}
