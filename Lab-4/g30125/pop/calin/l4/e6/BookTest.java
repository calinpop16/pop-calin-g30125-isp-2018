package g30125.pop.calin.l4.e6;

import static org.junit.Assert.*;

import org.junit.Test;

public class BookTest {

	@Test
	public void test() {
		Author[] a=new Author[3];
		a[0]=new Author("autor1","email1",'m');
		
		a[1]=new Author("autor2","email2",'m');
		
		a[2]=new Author("autor3","email3",'m');
	
		

		Book b1=new Book("carte1",a,220.0,3);
		assertEquals(b1.toString(),"book-carte1 by 3 authors");
	}


}
