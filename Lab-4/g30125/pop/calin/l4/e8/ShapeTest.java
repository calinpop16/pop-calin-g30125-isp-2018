package g30125.pop.calin.l4.e8;

import static org.junit.Assert.*;

import org.junit.Test;

public class ShapeTest {

	@Test
	public void test() {
		Circle c=new Circle(1,"black",true);
		assertEquals(c.toString(),"A Circle with radius=1.0,which is a subclass of A shape with color of black and filled");
	}
	
	@Test
	public void testRectangle() {
		Rectangle r=new Rectangle(1,2,"blue",true);
		assertEquals(r.toString(),"A Rectangle width=1.0 and length=2.0, which is a subclass of A shape with color of blue and filled");
	}
	@Test
	public void testSquare() {
		Square s=new Square(3,"red",false);
		assertEquals(s.toString(),"A Square with side=0.0, which is a subclass of A Rectangle width=3.0 and length=3.0, which is a subclass of A shape with color of red and not filled");
	}
	

}
