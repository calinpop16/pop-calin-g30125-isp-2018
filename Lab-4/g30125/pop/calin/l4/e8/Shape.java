package g30125.pop.calin.l4.e8;

public class Shape {
	private String color="red";
    private boolean filled=true;
    Shape(){
        this.color="green";
        this.filled=true;
    }
    Shape(String color, boolean filled){
        this.color=color;
        this.filled=filled;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isFilled() {
        return filled;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }
    public String toString() {
    	if(this.filled==true)
        return "A shape with color of " +this.color+" and filled";
    	else 
    		 return "A shape with color of " +this.color+" and not filled";
               
    }


}
