package g30125.pop.calin.l4.e2;

import static org.junit.Assert.*;

import org.junit.Test;

public class MyPointTest {

	MyPoint p1 = new MyPoint();
	MyPoint p2 = new MyPoint();
	MyPoint p3 = new MyPoint(2, 3);
	@Test
	public void testXY() {
		p2.setXY(5, 7);
		assertEquals(p2.getX(),5);
		assertEquals(p2.getY(),7);
	}
	@Test
	public void testX() {
		p1.setX(1);
		assertEquals(p1.getX(),1);

	}
	@Test
	public void testY() {
		p1.setY(3);
		assertEquals(p1.getY(),3);

	}
	@Test 
	public void testDistance() {
		p2.setXY(-1,5);
		assertEquals(p2.distance(2,1),5,0.01);
	}


}
