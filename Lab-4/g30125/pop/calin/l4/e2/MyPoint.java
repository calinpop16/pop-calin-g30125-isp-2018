package g30125.pop.calin.l4.e2;

import static java.lang.Math.sqrt;

public class MyPoint {
				private int x,y;
				
				int getX() //getter pentru x
				{
					return x;
				}
				
				int getY()// getter pentru y
				{
					return y;
				}
				
				public MyPoint () {//constructor fara parametrii
					this.x=0;
					this.y=0;
					
				}
			
				public MyPoint(int x,int y) {//constructor cu parametrii
					this.x=x;
					this.y=y;
				}
			
				public void setX(int x) {//setter pentru x
					this.x = x;
				}
				
				
				public void setY(int y) {//setter pentru y
					this.y = y;
				}
				
				public void setXY(int x, int y) {//metoda pentru setarea lui x si y 
					this.x=x;
					this.y=y;
				}
				
				public String toString() {// metoda toString
					return "("+this.x+","+this.y+")";
				}
				
				public double distance(int x, int y) {//metoda pentru distanta de la un punct la altul 
					return sqrt((this.x-x)*(this.x-x)+(this.y-y)*(this.y-y));
				}
				
				public double distance (MyPoint another) {
					return sqrt((this.x-another.x)*(this.x-another.x)+(this.y-another.y)*(this.y-another.y));
				}
				
}