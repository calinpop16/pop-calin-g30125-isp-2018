package g30125.pop.calin.l4.e5;

public class Book {
	private String name;
	private Author author;
	private double price;
	private int qtyInStock;
	
	Book(String name,Author author,Double price){
		this.name=name;
		this.author=author;
		this.price=price;
		
	}
	
	Book(String name,Author author,Double price,int qtyInStock){
		this.name=name;
		this.author=author;
		this.price=price;
		this.qtyInStock=qtyInStock;
		
	}
	
	public String get_name() {
		return this.name;
	}
	public Author get_author()
	{
		return this.author;
	}
	public double get_price() {
		return this.price;
	}
	
	public void set_price(double price) {
		this.price=price;
	}
	public int get_qtyInStock() {
		return this.qtyInStock;
	}
	public void set_qtyInStock(int qtyInStock) {
		this.qtyInStock=qtyInStock;
	}
	public String toString() {
		return "book-"+get_name()+" by author "+get_author().name+"("+get_author().gender+") email "+get_author().email;
	}


}
