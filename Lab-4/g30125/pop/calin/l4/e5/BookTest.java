package g30125.pop.calin.l4.e5;

import static org.junit.Assert.*;

import org.junit.Test;

public class BookTest {

	@Test
	public void test() {
		Author a=new Author("autor1","email1",'m');
		Book b1=new Book("carte1",a,220.0);
		assertEquals(b1.toString(),"book-carte1 by author autor1(m) email email1");
	}


}
