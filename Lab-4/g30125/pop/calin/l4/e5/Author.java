package g30125.pop.calin.l4.e5;

public class Author {
	protected String name;
	protected String email;
	protected char gender;
	public Author(String name,String email, char gender){
		this.name=name;
		this.email=email;
		this.gender=gender;
	}
	String get_name()
	{
		return this.name;
	}
	String get_email()
	{
		return this.email;
	}
	
	char get_gender()
	{
		return this.gender;
	}

}
