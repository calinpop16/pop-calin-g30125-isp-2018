package g30125.pop.calin.l4.e3;
import java.lang.Math;
public class Circle {
	private double radius;
	private String color;
	double getRadius(){
		return radius;
	}
	Circle (){
		radius=1.0;
		color="red";
	}
	Circle(double radius1){
		radius=radius1;
	}
	
	double getArea() {
		return Math.PI*getRadius()*getRadius();
	}

}
